package main

import (
	"code.afis.me/modules/go/websocket/clients"
	"code.afis.me/modules/go/websocket/interfaces"
	"context"
	"fmt"
	"os"
	"time"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	ws := clients.NewClients()
	go func() {
		ws.New(&interfaces.WebsocketClientOption{
			NoOfThreads: 1,
			Prefix:      "test",
			SocketURL: interfaces.ClientURL{
				Host:   "demo.piesocket.com",
				Scheme: "wss",
				Path:   fmt.Sprintf("/v3/channel_123?notify_self&api_key=%s", os.Getenv("PIESOCKET_API_KEY")),
			},
			TimeOutMsg:    15 * time.Second,
			CheckInterval: 1000 * time.Millisecond,
			CheckThread:   interfaces.FirstOnly,
			OnConnect: func(s string, i int) {
				fmt.Printf("websocket connected\n")
				ws.Send([]byte("Hello From Websocket"))
			},
			OnClose: func(s string, i int, err error) {
				fmt.Printf("websocket close, %s\n", err.Error())
				cancel()
			},
			OnError: func(s string, i int, err error) {
				fmt.Printf("websocket error, %s\n", err.Error())
				cancel()
			},
			OnMessage: func(s string, i int, msg *[]byte) {
				fmt.Printf("receive message, %s\n", string(*msg))
				if string(*msg) == "Hello From Websocket" {
					ws.Destroy()
				}
			},
		})
	}()

	for {
		if ctx.Err() != nil {
			break
		}
	}
}
