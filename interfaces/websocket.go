package interfaces

import (
	"time"

	"code.afis.me/modules/go/websocket/counter"
	"code.afis.me/modules/go/websocket/encoding/gob"
)

// IWebsocketClient -
type IWebsocketClient interface {
	New(*WebsocketClientOption)
	Send(msg []byte)
	SendID(idx int, msg []byte)
	SendGob(interface{})
	SendGobID(idx int, msg interface{})
	SendJSON(interface{})
	SendJSONID(idx int, msg interface{})
	ChangeURL(ClientURL, bool, ClientURL)
	Close(int)
	CloseAll()
	Destroy()
	DestroyId(id int)
}

// WebsocketClient -
type WebsocketClient struct {
	IWebsocketClient
}

// WebsocketClientOption -
type WebsocketClientOption struct {
	NoOfThreads       int
	Prefix            string
	TimeOutMsg        time.Duration
	OnTickerTimeOut   func(string, int, time.Time, time.Duration)
	CheckInterval     time.Duration
	CheckThread       CheckThread
	OnConnect         func(string, int)
	OnMessage         func(string, int, *[]byte)
	OnClose           func(string, int, error)
	OnError           func(string, int, error)
	OnDestroy         func(string, int, error)
	UseProxy          bool
	SocketURL         ClientURL
	ProxyURL          ClientURL
	TimeOutReconnect  time.Duration
	DialTimeout       time.Duration
	UserAgent         string
	Counter           *counter.RateCounter
	EnableCompression gob.CompressType
}

// ClientURL -
type ClientURL struct {
	Host     string
	Scheme   string
	Path     string
	Port     int
	Origin   string
	Username string
	Password string
	WithAuth bool
}

// CheckThread -
type CheckThread int

const (
	//FirstOnly -
	FirstOnly CheckThread = iota
	//All -
	All
)
