package counter

import (
	"io"
	"sync/atomic"
	"time"
)

// A Counter is a thread-safe counter implementation
type Counter struct {
	value int64
}

// Incr method increments the counter by some value
func (c *Counter) Incr(val int64) {
	atomic.AddInt64(&c.value, val)
}

// Reset -
func (c *Counter) Reset() {
	atomic.StoreInt64(&c.value, 0)
}

// RateCounter -
type RateCounter struct {
	Reader   *Counter
	Writer   *Counter
	Callback func(int64, int64)
}

// NewRateCounter -
func NewRateCounter(cb func(int64, int64)) *RateCounter {
	return &RateCounter{
		Reader:   &Counter{},
		Writer:   &Counter{},
		Callback: cb,
	}
}

// Run -
func (cc *RateCounter) Run() *RateCounter {

	go func() {
		ticker := time.NewTicker(1 * time.Second)
		for {
			select {
			case t := <-ticker.C:
				t.Second()
				r := atomic.LoadInt64(&cc.Reader.value)
				w := atomic.LoadInt64(&cc.Writer.value)
				go cc.Callback(r, w)
				cc.Reader.Reset()
				cc.Writer.Reset()
			}
		}
	}()

	return cc
}

// Reader counts the bytes read through it.
type Reader struct {
	r io.Reader
	c *RateCounter
}

// NewReader makes a new Reader that counts the bytes
// read through it.
func NewReader(r io.Reader, c *RateCounter) *Reader {
	return &Reader{
		r: r,
		c: c,
	}
}
func (r *Reader) Read(p []byte) (n int, err error) {
	n, err = r.r.Read(p)
	if r.c != nil {
		r.c.Reader.Incr(int64(n))
	}

	return
}

// Writer -
type Writer struct {
	w io.WriteCloser
	c *RateCounter
}

// NewWriter -
func NewWriter(w io.WriteCloser, c *RateCounter) *Writer {
	return &Writer{
		w: w,
		c: c,
	}
}

func (w *Writer) Write(p []byte) (n int, err error) {
	n, err = w.w.Write(p)
	if w.c != nil {
		w.c.Writer.Incr(int64(n))
	}
	return
}

// Close -
func (w *Writer) Close() error {
	return w.w.Close()
}
