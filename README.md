####  Modules Websocket
Websocket Client Management

#### Base on lib
- github.com/gorilla/websocket

##### Installation
```sh
go get code.afis.me/modules/go/websocket
```

##### Example
```shell
PIESOCKET_API_KEY="<TOKEN>" go run examples/example.go
```
