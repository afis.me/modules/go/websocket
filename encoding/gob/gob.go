package gob

import (
	"bytes"
	"compress/gzip"
	"compress/zlib"
	"encoding/gob"
	"fmt"
)

// CompressType -
type CompressType int

const (
	CompressTypeNone CompressType = iota
	CompressTypeZlib
	CompressTypeGzip
)

// Encoder -
type Encoder struct {
	CompressType CompressType
}

// CompressName -
func CompressName(cp CompressType) (name string) {
	switch cp {
	case CompressTypeZlib:
		return "zlib"
	case CompressTypeGzip:
		return "gzip"
	}
	return "none"
}

// GetCompressType -
func GetCompressType(cp string) (name CompressType) {
	switch cp {
	case "zlib":
		return CompressTypeZlib
	case "gzip":
		return CompressTypeGzip
	}
	return CompressTypeNone
}

// NewEncoder -
func NewEncoder(cp CompressType) *Encoder {
	return &Encoder{CompressType: cp}
}

// NoCompress -
func (cc *Encoder) noCompress(data interface{}) []byte {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(data)
	defer buf.Reset()
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return buf.Bytes()
}

// Encode -
func (cc *Encoder) Encode(data interface{}) []byte {

	if cc.CompressType == CompressTypeNone {
		return cc.noCompress(data)
	}

	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	defer buf.Reset()

	if err := enc.Encode(data); err != nil {
		fmt.Println(err)
		return nil
	}

	var b bytes.Buffer
	defer b.Reset()

	if cc.CompressType == CompressTypeZlib {
		w := zlib.NewWriter(&b)
		defer w.Close()
		if _, err := w.Write(buf.Bytes()); err != nil {
			fmt.Println(err)
			return nil
		}
		if err := w.Flush(); err != nil {
			fmt.Println(err)
			return nil
		}
	}

	if cc.CompressType == CompressTypeGzip {
		w := gzip.NewWriter(&b)
		defer w.Close()
		if _, err := w.Write(buf.Bytes()); err != nil {
			fmt.Println(err)
			return nil
		}
		if err := w.Flush(); err != nil {
			fmt.Println(err)
			return nil
		}
	}

	return b.Bytes()
}

// Decoder -
type Decoder struct {
	CompressType CompressType
}

// NewDecoder -
func NewDecoder(cp CompressType) *Decoder {
	return &Decoder{CompressType: cp}
}

// NoCompress -
func (cc *Decoder) noCompress(raw []byte, data interface{}) (err error) {
	buf := bytes.NewBuffer(raw)
	err = gob.NewDecoder(buf).Decode(data)
	defer buf.Reset()
	if err != nil {
		return err
	}

	buf.Reset()
	return err
}

// Decode -
func (cc *Decoder) Decode(raw []byte, data interface{}) error {

	if cc.CompressType == CompressTypeNone {
		return cc.noCompress(raw, data)
	}

	b := bytes.NewReader(raw)

	if cc.CompressType == CompressTypeZlib {
		r, err := zlib.NewReader(b)
		defer r.Close()
		if err != nil {
			return err
		}
		return gob.NewDecoder(r).Decode(data)
	}

	if cc.CompressType == CompressTypeGzip {
		r, err := gzip.NewReader(b)
		defer r.Close()
		if err != nil {
			return err
		}
		return gob.NewDecoder(r).Decode(data)
	}

	return nil
}
