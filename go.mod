module code.afis.me/modules/go/websocket

go 1.19

require (
	github.com/gorilla/websocket v1.5.1
	golang.org/x/net v0.18.0
)
