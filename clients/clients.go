package clients

import (
	"crypto/tls"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"runtime/debug"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"code.afis.me/modules/go/websocket/encoding/gob"

	"encoding/json"

	"code.afis.me/modules/go/websocket/counter"
	"code.afis.me/modules/go/websocket/interfaces"
	"github.com/gorilla/websocket"
	"golang.org/x/net/proxy"
)

// NewClients -
func NewClients() *interfaces.WebsocketClient {
	return &interfaces.WebsocketClient{IWebsocketClient: &Clients{AlreadyDestroyIndex: map[int]bool{}}}
}

// Clients -
type Clients struct {
	Options             *interfaces.WebsocketClientOption
	Socket              []*clients
	CurrentIndex        int
	CountThread         int64
	AlreadyDestroyIndex map[int]bool
	sync.RWMutex
}

func (cc *Clients) removeSocket(s []*clients, index int) []*clients {
	return append(s[:index], s[index+1:]...)
}

// New -
func (cc *Clients) New(opt *interfaces.WebsocketClientOption) {
	cc.Options = opt

	go func() {
		time.Sleep(1 * time.Second)
		for i := 1; i <= cc.Options.NoOfThreads; i++ {
			go func(i int) {
				cl := &clients{Control: cc}
				cl.New()
			}(i)

		}
	}()

}

func (cc *Clients) choose() (key *clients, err error) {
	if len(cc.Socket) == 0 {
		return key, fmt.Errorf("no client available to handle this request")
	}
	idx := cc.CurrentIndex % len(cc.Socket)
	cc.CurrentIndex++
	return cc.Socket[idx], err
}

// Send -
func (cc *Clients) Send(msg []byte) {
	if cl, err := cc.choose(); err == nil {
		go cl.Write.Write(msg)
	}
}

// SendID -
func (cc *Clients) SendID(idx int, msg []byte) {
	for _, cl := range cc.Socket {
		if cl.ThreadID == idx {
			go cl.Write.Write(msg)
			break
		}
	}

}

// SendGob -
func (cc *Clients) SendGob(msg interface{}) {
	if cl, err := cc.choose(); err == nil {
		data := gob.NewEncoder(cc.Options.EnableCompression).Encode(msg)
		go cl.Write.Write(data)
	}
}

// SendGobID -
func (cc *Clients) SendGobID(idx int, msg interface{}) {
	for _, cl := range cc.Socket {
		if cl.ThreadID == idx {
			data := gob.NewEncoder(cc.Options.EnableCompression).Encode(msg)
			go cl.Write.Write(data)
			break
		}
	}
}

// SendJSON -
func (cc *Clients) SendJSON(msg interface{}) {
	if data, err := json.Marshal(msg); err == nil {
		if cl, err := cc.choose(); err == nil {
			go cl.Write.Write(data)
		}
	}
}

// SendJSONID -
func (cc *Clients) SendJSONID(idx int, msg interface{}) {
	//fmt.Println(idx, msg)
	if data, err := json.Marshal(msg); err == nil {
		for _, cl := range cc.Socket {
			if cl.ThreadID == idx {
				go cl.Write.Write(data)
				break
			}
		}

	}
}

// Destroy -
func (cc *Clients) Destroy() {
	cc.RLock()
	sockets := cc.Socket
	cc.RUnlock()

	for _, s := range sockets {
		cc.Lock()
		cc.AlreadyDestroyIndex[s.ThreadID] = true
		cc.Unlock()
		s.Destroy()
	}

	for i := 1; i <= cc.Options.NoOfThreads; i++ {
		cc.Lock()
		cc.AlreadyDestroyIndex[i] = true
		cc.Unlock()
		cc.Close(i)
	}

}

func (cc *Clients) DestroyId(id int) {
	cc.RLock()
	sockets := cc.Socket
	cc.RUnlock()

	for _, s := range sockets {
		if s.ThreadID == id {
			cc.Lock()
			cc.AlreadyDestroyIndex[id] = true
			cc.Unlock()
			s.Destroy()
			cc.Close(id)
		}
	}
}

// Close -
func (cc *Clients) Close(id int) {
	cc.RLock()
	sockets := cc.Socket
	cc.RUnlock()

	for _, s := range sockets {
		if s.ThreadID == id {
			cc.RLock()
			client := s.Client
			if client != nil {
				_ = s.Client.Close()
			}
			cc.RUnlock()
		}
	}
}

// CloseAll -
func (cc *Clients) CloseAll() {
	for _, s := range cc.Socket {
		if s.Client != nil {
			_ = s.Client.Close()
		}
	}
}

// ChangeURL -
func (cc *Clients) ChangeURL(ad interfaces.ClientURL, useProxy bool, pr interfaces.ClientURL) {
	oldConfig := cc.Options.SocketURL
	cc.Options.SocketURL = ad
	cc.Options.UseProxy = useProxy
	cc.Options.ProxyURL = pr
	if len(cc.Options.SocketURL.Scheme) == 0 {
		cc.Options.SocketURL.Scheme = oldConfig.Scheme
	}

	if len(cc.Options.SocketURL.Path) == 0 {
		cc.Options.SocketURL.Path = oldConfig.Path
	}

}

type clients struct {
	Control     *Clients
	ThreadID    int
	ID          string
	Client      *websocket.Conn
	Header      http.Header
	LastMsgTime time.Time
	SubChannel  *SubChannel
	Write       *writeMessage
	Read        *readMessage
	sync.RWMutex
}

type writeMessage struct {
	Conn    *websocket.Conn
	Counter *counter.RateCounter
	sync.Mutex
}

type readMessage struct {
	Conn    *websocket.Conn
	Counter *counter.RateCounter
	sync.RWMutex
}

func (cc *readMessage) Message() (messageType int, p []byte, err error) {

	cc.Lock()
	var r io.Reader
	messageType, r, err = cc.Conn.NextReader()
	if err != nil {
		return messageType, nil, err
	}
	p, err = io.ReadAll(counter.NewReader(r, cc.Counter))
	cc.Unlock()

	return messageType, p, err
}

func (cc *writeMessage) Write(data []byte) {
	cc.Lock()
	defer cc.Unlock()
	_ = cc.Message(websocket.BinaryMessage, data)

}

func (cc *writeMessage) Message(messageType int, data []byte) error {
	if cc.Conn == nil {
		return fmt.Errorf("client has been destroy")
	}

	w, err := cc.Conn.NextWriter(messageType)
	if err != nil {
		return err
	}

	writer := counter.NewWriter(w, cc.Counter)
	if _, err = writer.Write(data); err != nil {
		return err
	}

	return writer.Close()
}

func (cc *clients) New() {
	atomic.AddInt64(&cc.Control.CountThread, 1)
	cc.ThreadID = int(atomic.LoadInt64(&cc.Control.CountThread))

	cc.Lock()
	cc.ID = fmt.Sprintf("%s:::%d", cc.Control.Options.Prefix, cc.ThreadID)
	cc.Unlock()

	_ = cc.Connect()

}

func (cc *clients) Connect() (err error) {
	timeOut := cc.Control.Options.DialTimeout
	if timeOut.Seconds() < 1 {
		timeOut = 5 * time.Second
	}

	var DefaultDialer *websocket.Dialer
	if !cc.Control.Options.UseProxy {
		DefaultDialer = &websocket.Dialer{
			HandshakeTimeout: timeOut,
			TLSClientConfig:  &tls.Config{InsecureSkipVerify: true},
		}
	}

	if cc.Control.Options.UseProxy {
		auth := &proxy.Auth{}
		if cc.Control.Options.ProxyURL.WithAuth {
			auth.User = cc.Control.Options.ProxyURL.Username
			auth.Password = cc.Control.Options.ProxyURL.Password
		}

		host := fmt.Sprintf("%s:%d", cc.Control.Options.ProxyURL.Host, cc.Control.Options.ProxyURL.Port)

		if dialSocksProxy, errs := proxy.SOCKS5("tcp", host, auth, &net.Dialer{Timeout: timeOut}); errs == nil {
			DefaultDialer = &websocket.Dialer{
				NetDial:          dialSocksProxy.Dial,
				HandshakeTimeout: timeOut,
				TLSClientConfig:  &tls.Config{InsecureSkipVerify: true},
			}
		} else {
			cc.Control.RLock()
			isDestroy := cc.Control.AlreadyDestroyIndex[cc.ThreadID]
			cc.Control.RUnlock()

			if !isDestroy {
				cc.OnError(errs)
			}
			return errs
		}

	}

	if cc.Header == nil {
		cc.Header = http.Header{}
	}

	if cc.Control.Options.EnableCompression != gob.CompressTypeNone {
		cc.Header.Set("Compression", gob.CompressName(cc.Control.Options.EnableCompression))
	}

	cc.Header.Set("Host", cc.Control.Options.SocketURL.Host)
	cc.Header.Set("Origin", cc.origin())
	cc.Header.Set("User-Agent", cc.Control.Options.UserAgent)
	if len(cc.Control.Options.Prefix) != 0 {
		cc.Header.Set("ClientID", cc.ID)
	}

	cc.Lock()
	cc.Client, _, err = DefaultDialer.Dial(cc.url(), cc.Header)
	cc.Unlock()

	if err != nil {
		cc.Control.RLock()
		isDestroy := cc.Control.AlreadyDestroyIndex[cc.ThreadID]
		cc.Control.RUnlock()

		if !isDestroy {
			cc.OnError(err)
		}
		return err
	}

	cc.Write = &writeMessage{
		Conn:    cc.Client,
		Counter: cc.Control.Options.Counter,
	}

	cc.Read = &readMessage{
		Conn:    cc.Client,
		Counter: cc.Control.Options.Counter,
	}

	cc.Lock()
	cc.LastMsgTime = time.Now()
	cc.Unlock()

	if cc.Control.Options.TimeOutMsg != 0 {

		var valid = true
		if cc.Control.Options.CheckThread == interfaces.FirstOnly {
			if cc.ThreadID != 1 {
				valid = false
			}
		}

		if cc.Control.Options.CheckThread == interfaces.All {
			valid = true
		}

		if valid {

			cc.Lock()
			cc.SubChannel = &SubChannel{}
			cc.SubChannel.channel = make(chan struct{})
			cc.Unlock()

			if cc.Control.Options.CheckInterval.Milliseconds() != 0 {

				cc.Lock()
				cc.SubChannel.ticker = time.NewTicker(cc.Control.Options.CheckInterval)
				cc.Unlock()

			} else {
				cc.Lock()
				cc.SubChannel.ticker = time.NewTicker(1 * time.Second)
				cc.Unlock()
			}

			go func() {
				cc.Lock()
				ticker := cc.SubChannel.ticker
				subChannel := cc.SubChannel
				cc.Unlock()

				for {
					select {
					case <-ticker.C:

						if cc.Control.Options.OnTickerTimeOut != nil {
							cc.Control.Options.OnTickerTimeOut(cc.ID, cc.ThreadID, cc.LastMsgTime, time.Since(cc.LastMsgTime))
						}

						if cc.Control.Options != nil {
							cc.RLock()
							lastTime := cc.LastMsgTime
							cc.RUnlock()

							if time.Since(lastTime).Seconds() > cc.Control.Options.TimeOutMsg.Seconds() {
								if cc.Client != nil {
									_ = cc.Client.Close()
								}
								return
							}
						}

					case <-subChannel.channel:
						if cc != nil {
							if subChannel != nil {
								subChannel.Close(func() {})
							}
						}
						return
					}
				}
			}()
		}

	}

	go func() {
		defer func(Client *websocket.Conn) {
			_ = Client.Close()
		}(cc.Client)
		for {

			cc.Lock()
			cc.LastMsgTime = time.Now()
			cc.Unlock()

			_, message, err := cc.Read.Message()

			if err != nil {

				cc.Control.RLock()
				isDestroy := cc.Control.AlreadyDestroyIndex[cc.ThreadID]
				cc.Control.RUnlock()

				if !isDestroy {
					cc.OnError(err)
				}

				if isDestroy {
					if cc.Control.Options.OnDestroy != nil {
						cc.Control.Options.OnDestroy(cc.ID, cc.ThreadID, fmt.Errorf("client has been destroy"))
					} else {
						cc.OnError(fmt.Errorf("client has been destroy"))
					}

				}

				break
			}

			if cc.Control.Options.OnMessage != nil {
				cc.Control.Options.OnMessage(cc.ID, cc.ThreadID, &message)
			}

		}
	}()

	cc.Control.Lock()
	cc.Control.Socket = append(cc.Control.Socket, cc)
	cc.Control.Unlock()

	if cc.Control.Options.OnConnect != nil {
		cc.Control.Options.OnConnect(cc.ID, cc.ThreadID)
	}

	return err

}

func (cc *clients) OnError(err error) {
	cc.Control.RLock()
	sockets := cc.Control.Socket
	cc.Control.RUnlock()

	var rIndex *int
	for i, s := range sockets {
		if s.ThreadID == cc.ThreadID {
			rIndex = &i
			break
		}
	}

	if rIndex != nil {
		cc.Control.Lock()
		cc.Control.Socket = cc.Control.removeSocket(cc.Control.Socket, *rIndex)
		cc.Control.Unlock()
	}

	var errors = err
	if cc.Client != nil {
		_ = cc.Client.Close()
	}

	var prefix string = ""
	if strings.Contains(fmt.Sprintf("%s", err), "socks") {
		prefix = "socks proxy : "
	}

	if strings.Contains(fmt.Sprintf("%s", err), "connect: connection refused") {
		errors = fmt.Errorf("%sconnect: connection refused", prefix)
	}

	if strings.Contains(fmt.Sprintf("%s", err), "connect: network is unreachable") {
		errors = fmt.Errorf("%sconnect: network is unreachable", prefix)
	}

	if strings.Contains(fmt.Sprintf("%s", err), "read: connection refused") {
		errors = fmt.Errorf("%sread: connection refused", prefix)
	}

	if strings.Contains(fmt.Sprintf("%s", err), "use of closed network connection") {
		errors = fmt.Errorf("%suse of closed network connection", prefix)

	}

	if strings.Contains(fmt.Sprintf("%s", err), "invalid username/password") {
		errors = fmt.Errorf("%sinvalid username/password", prefix)

	}

	if cc.Control.Options.OnError != nil {
		cc.Control.Options.OnError(cc.ID, cc.ThreadID, errors)
	}

	if cc.SubChannel != nil {
		cc.SubChannel.Close(func() {})
	}

	cc.Control.RLock()
	isDestroy := cc.Control.AlreadyDestroyIndex[cc.ThreadID]
	//prefixClient := cc.Control.Options.Prefix
	cc.Control.RUnlock()

	//fmt.Println(prefixClient, cc.ThreadID, isDestroy)

	if !isDestroy {
		go func() {
			var tss time.Duration
			if cc.Control.Options.TimeOutReconnect.Seconds() == 0 {
				tss = 1 * time.Second
			} else {
				tss = cc.Control.Options.TimeOutReconnect
			}
			time.Sleep(tss)
			_ = cc.Connect()
		}()
	}

}

func (cc *clients) Destroy() {
	log.Println(cc.ID, "Destroy request for thread id", cc.ThreadID)

	if cc.SubChannel != nil {
		cc.SubChannel.Close(func() {})
	}

	if cc.Client != nil {
		_ = cc.Client.Close()
	} else {
		cc.Client = nil
	}
}

func (cc *clients) prefix() string {
	prefix := cc.Control.Options.SocketURL.Scheme
	if prefix == "https" {
		prefix = "wss"
	}
	if prefix == "http" {
		prefix = "ws"
	}
	return prefix
}

func (cc *clients) url() string {
	u := fmt.Sprintf("%s://%s%s", cc.prefix(), cc.Control.Options.SocketURL.Host, cc.Control.Options.SocketURL.Path)
	if cc.Control.Options.SocketURL.Port != 0 {
		u = fmt.Sprintf("%s://%s:%d%s", cc.prefix(), cc.Control.Options.SocketURL.Host, cc.Control.Options.SocketURL.Port, cc.Control.Options.SocketURL.Path)
	}
	return u
}

func (cc *clients) origin() string {
	hostorigin := cc.prefix()
	if hostorigin == "wss" {
		hostorigin = "https"
	}

	if hostorigin == "ws" {
		hostorigin = "http"
	}

	origin := fmt.Sprintf("%s://%s", cc.Control.Options.SocketURL.Scheme, cc.Control.Options.SocketURL.Host)

	if cc.Control.Options.SocketURL.Port != 0 {
		origin = fmt.Sprintf("%s://%s:%d", cc.Control.Options.SocketURL.Scheme, cc.Control.Options.SocketURL.Host, cc.Control.Options.SocketURL.Port)
	}

	return origin
}

// SubChannel -
type SubChannel struct {
	channel   chan struct{}
	closeOnce sync.Once
	ticker    *time.Ticker
	sync.Mutex
}

// Close -
func (cc *SubChannel) Close(callback func()) {
	defer func() {
		if r := recover(); r != nil {
			debug.PrintStack()
		}
	}()

	cc.closeOnce.Do(func() {
		if cc.channel != nil {
			close(cc.channel)
		}

		if cc.ticker != nil {
			cc.ticker.Stop()
		}
	})
}
